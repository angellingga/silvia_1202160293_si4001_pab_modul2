package com.example.android.silvia_1202160293_si4001_pab_modul2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Summary extends AppCompatActivity {

    TextView viewTujuan, viewTglPergi,
            viewWaktupergi, viewTglPulang,
            viewWaktuPulang, viewJumlahTiket,
            viewTotalharga,viewSaldo,
            viewSisaSaldo;

    Button konfirmasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        viewTujuan = (TextView)findViewById(R.id.viewTujuan);
        viewTglPergi = (TextView)findViewById(R.id.viewTglPergi);
        viewWaktupergi = (TextView)findViewById(R.id.viewWaktuPergi);
        viewTglPulang = (TextView)findViewById(R.id.viewTglPulang);
        viewWaktuPulang= (TextView)findViewById(R.id.viewWaktuPulang);
        viewJumlahTiket = (TextView)findViewById(R.id.viewJumlahTiket);
        viewTotalharga = (TextView)findViewById(R.id.viewTotalHarga);
        viewSaldo = (TextView)findViewById(R.id.viewSaldo);
        viewSisaSaldo = (TextView)findViewById(R.id.viewSisaSaldo);
        konfirmasi = (Button)findViewById(R.id.btnKonfirmasi);

        Intent in = getIntent();

        String tujuan = in.getStringExtra("daerahTujuan");
        String tanggalpergi = in.getStringExtra("tanggalBerangkat");
        String waktupergi = in.getStringExtra("waktuBerangkat");
        String tanggalpulang = in.getStringExtra("tanggalPulang");
        String waktupulang = in.getStringExtra("waktuPulang");
        String jumlahTiket = in.getStringExtra("jumlahTiket");
        String hargaTotal = in.getStringExtra("hargaTotal");
        String saldo = in.getStringExtra("saldo");
        String sisasaldo = in.getStringExtra("sisaSaldo");

        viewTujuan.setText(tujuan);
        viewTglPergi.setText(tanggalpergi);
        viewWaktupergi.setText(waktupergi);
        viewTglPulang.setText(tanggalpulang);
        viewWaktuPulang.setText(waktupulang);
        viewJumlahTiket.setText(jumlahTiket);
        viewTotalharga.setText(hargaTotal);
        viewSaldo.setText(saldo);
        viewSisaSaldo.setText(sisasaldo);

        konfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Terimakasih Telah Menggunakan Jasa Kami!", Toast.LENGTH_LONG).show();
                finish();
                moveTaskToBack(true);
            }
        });

    }
}
