package com.example.android.silvia_1202160293_si4001_pab_modul2;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    private Button buttonDatePicker, buttonTimePicker, buttonBeliTiket, buttonTopUp;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private DatePickerDialog.OnDateSetListener mDateSetListener2;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener;
    private TimePickerDialog.OnTimeSetListener mTimeSetListener2;
    private EditText txtSaldo, txtJumlahTiket;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Switch switchPulangpergi;
    private Spinner spinnerTujuan;
    private Button buttonDatePickerPulang, buttonTimePickerPulang;
    private int hargaTiket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* variable */
        buttonTopUp = (Button)findViewById(R.id.topup_btn);
        buttonDatePicker = (Button)findViewById(R.id.tanggal_btn);
        buttonTimePicker = (Button)findViewById(R.id.waktu_btn);
        buttonDatePickerPulang = (Button)findViewById(R.id.tglpulang_btn);
        buttonTimePickerPulang = (Button)findViewById(R.id.waktupulang_btn);
        buttonBeliTiket = (Button)findViewById(R.id.beli_btn) ;
        spinnerTujuan = (Spinner)findViewById(R.id.tujuan_spn);
        txtSaldo = (EditText) findViewById(R.id.saldo_edt);
        txtJumlahTiket = (EditText) findViewById(R.id.jmlh_edt);
        switchPulangpergi = (Switch)findViewById(R.id.pp_switch);

        /* method memlilih waktu dan tanggal */
        buttonDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                mYear = cal.get(Calendar.YEAR);
                mMonth = cal.get(Calendar.MONTH);
                mDay = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dateDialog = new DatePickerDialog(MainActivity.this,
                        mDateSetListener,
                        mYear,mMonth,mDay);
                dateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dateDialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = mMonth + 1;
                String date = dayOfMonth + "/" + month + "/" + year;
                buttonDatePicker.setText(date);
            }
        };

        buttonTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                mHour = cal.get(Calendar.HOUR_OF_DAY);
                mMinute = cal.get(Calendar.MINUTE);

                TimePickerDialog timeDialog = new TimePickerDialog(MainActivity.this,
                        mTimeSetListener,
                        mHour, mMinute, false);
                timeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                timeDialog.show();
            }
        });

        mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String time = hourOfDay + " : " + minute;
                buttonTimePicker.setText(time);
            }
        };

        /* method saat switch on atau off */
        switchPulangpergi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (switchPulangpergi.isChecked() == true){

                    buttonDatePickerPulang.setVisibility(View.VISIBLE);
                    buttonTimePickerPulang.setVisibility(View.VISIBLE);

                    buttonDatePickerPulang.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar cal = Calendar.getInstance();
                            mYear = cal.get(Calendar.YEAR);
                            mMonth = cal.get(Calendar.MONTH);
                            mDay = cal.get(Calendar.DAY_OF_MONTH);

                            DatePickerDialog dateDialog = new DatePickerDialog(MainActivity.this,
                                    mDateSetListener2,
                                    mYear,mMonth,mDay);
                            dateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                            dateDialog.show();
                        }
                    });

                    mDateSetListener2 = new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            month = mMonth + 1;
                            String date = dayOfMonth + "/" + month + "/" + year;
                            buttonDatePickerPulang.setText(date);
                        }
                    };

                    buttonTimePickerPulang.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar cal = Calendar.getInstance();
                            mHour = cal.get(Calendar.HOUR_OF_DAY);
                            mMinute = cal.get(Calendar.MINUTE);

                            TimePickerDialog timeDialog = new TimePickerDialog(MainActivity.this,
                                    mTimeSetListener2,
                                    mHour, mMinute, false);
                            timeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            timeDialog.show();
                        }
                    });
                    mTimeSetListener2 = new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            String time = hourOfDay + " : " + minute;
                            buttonTimePickerPulang.setText(time);
                        }
                    };

                }
                else if(!switchPulangpergi.isChecked()){
                    buttonDatePickerPulang.setVisibility(View.GONE);
                    buttonTimePickerPulang.setVisibility(View.GONE);
                    buttonDatePickerPulang.setText("PILIH TANGGAL");
                    buttonTimePickerPulang.setText("PILIH WAKTU");

                    buttonDatePicker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar cal = Calendar.getInstance();
                            mYear = cal.get(Calendar.YEAR);
                            mMonth = cal.get(Calendar.MONTH);
                            mDay = cal.get(Calendar.DAY_OF_MONTH);

                            DatePickerDialog dateDialog = new DatePickerDialog(MainActivity.this,
                                    mDateSetListener,
                                    mYear,mMonth,mDay);
                            dateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                            dateDialog.show();
                        }
                    });

                    mDateSetListener = new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            month = mMonth + 1;
                            String date = dayOfMonth + "/" + month + "/" + year;
                            buttonDatePicker.setText(date);
                        }
                    };

                    buttonTimePicker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Calendar cal = Calendar.getInstance();
                            mHour = cal.get(Calendar.HOUR_OF_DAY);
                            mMinute = cal.get(Calendar.MINUTE);

                            TimePickerDialog timeDialog = new TimePickerDialog(MainActivity.this,
                                    mTimeSetListener,
                                    mHour, mMinute, false);
                            timeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            timeDialog.show();
                        }
                    });

                    mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            String time = hourOfDay + " : " + minute;
                            buttonTimePicker.setText(time);
                        }
                    };
                }
            }
        });

        /* method untuk topup */
        buttonTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    Integer jumlahSaldo =Integer.parseInt(txtSaldo.getText().toString());

                    if(jumlahSaldo > 0){
                        Toast.makeText(getApplicationContext(), "Saldo Berhasil Ditambahkan!", Toast.LENGTH_LONG).show();
                    }
                    else {
                        AlertDialog.Builder alertBuild = new AlertDialog.Builder(MainActivity.this);
                        alertBuild.setMessage("Mohon Isi Saldo Anda!").setNegativeButton(
                                "Coba Lagi", null).create().show();
                    }
                }
                catch(Exception e){
                    AlertDialog.Builder alertBuild = new AlertDialog.Builder(MainActivity.this);
                    alertBuild.setMessage("Mohon Isi Saldo Anda Terlebih Dahulu!").setNegativeButton(
                            "Coba Lagi", null).create().show();
                }
            }
        });


        /* method beli tiket */
        buttonBeliTiket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer a = 0;
                Integer hargaTotal = 0;
                if(switchPulangpergi.isChecked()){
                    a = 2;
                }
                else if(!switchPulangpergi.isChecked()){
                    a = 1;
                }

                try{
                    Integer jumlahSaldo = Integer.parseInt(txtSaldo.getText().toString());
                    Integer jumlahTiket = Integer.parseInt(txtJumlahTiket.getText().toString());

                    Intent intent = new Intent(MainActivity.this, Summary.class);

                    if(spinnerTujuan.getSelectedItem().equals("Jakarta (Rp 100.000)")){
                        hargaTiket = 100000;
                        hargaTotal = a*hargaTiket*jumlahTiket;
                        Integer sisaSaldo = jumlahSaldo - (hargaTotal);
                        if(jumlahSaldo - (hargaTotal) < 0){
                            AlertDialog.Builder alertBuild = new AlertDialog.Builder(MainActivity.this);
                            alertBuild.setMessage("Mohon Maaf, Saldo Anda Tidak Cukup!").setNegativeButton(
                                    "Coba Lagi", null).create().show();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Tiket Anda Sudah Berhasil Dipesan!", Toast.LENGTH_LONG).show();
                            intent.putExtra("daerahTujuan", spinnerTujuan.getSelectedItem().toString());
                            intent.putExtra("tanggalBerangkat", buttonDatePicker.getText().toString());
                            intent.putExtra("waktuBerangkat", buttonTimePicker.getText().toString());
                            intent.putExtra("tanggalPulang", buttonDatePickerPulang.getText().toString());
                            intent.putExtra("waktuPulang", buttonTimePickerPulang.getText().toString());
                            intent.putExtra("jumlahTiket", txtJumlahTiket.getText().toString());
                            intent.putExtra("hargaTotal", hargaTotal.toString());
                            intent.putExtra("saldo", txtSaldo.getText().toString());
                            intent.putExtra("sisaSaldo", sisaSaldo.toString());
                            MainActivity.this.startActivity(intent);
                        }
                    }

                    else if(spinnerTujuan.getSelectedItem().equals("Bandung (Rp 70.000)")){
                        hargaTiket = 70000;
                        hargaTotal = a*hargaTiket*jumlahTiket;
                        Integer sisaSaldo = jumlahSaldo - (hargaTotal);
                        if(jumlahSaldo - (hargaTotal) < 0){
                            AlertDialog.Builder alertBuild = new AlertDialog.Builder(MainActivity.this);
                            alertBuild.setMessage("Mohon Maaf, Saldo Anda Tidak cukup!").setNegativeButton(
                                    "Coba Lagi", null).create().show();
                        }

                        else {
                            Toast.makeText(getApplicationContext(), "Tiket Anda Sudah Berhasil Dipesan !", Toast.LENGTH_LONG).show();
                            intent.putExtra("daerahTujuan", spinnerTujuan.getSelectedItem().toString());
                            intent.putExtra("tanggalBerangkat", buttonDatePicker.getText().toString());
                            intent.putExtra("waktuBerangkat", buttonTimePicker.getText().toString());
                            intent.putExtra("tanggalPulang", buttonDatePickerPulang.getText().toString());
                            intent.putExtra("waktuPulang", buttonTimePickerPulang.getText().toString());
                            intent.putExtra("jumlahTiket", txtJumlahTiket.getText().toString());
                            intent.putExtra("hargaTotal", hargaTotal.toString());
                            intent.putExtra("saldo", txtSaldo.getText().toString());
                            intent.putExtra("sisaSaldo", sisaSaldo.toString());
                            MainActivity.this.startActivity(intent);
                        }
                    }
                    else if(spinnerTujuan.getSelectedItem().equals("Tanggerang (Rp 130.000)")){
                        hargaTiket = 130000;
                        hargaTotal = a*hargaTiket*jumlahTiket;
                        Integer sisaSaldo = jumlahSaldo - (hargaTotal);
                        if(jumlahSaldo - (hargaTotal) < 0){
                            AlertDialog.Builder alertBuild = new AlertDialog.Builder(MainActivity.this);
                            alertBuild.setMessage("Mohon Maaf, Saldo Anda Tidak cukup!").setNegativeButton(
                                    "Coba Lagi", null).create().show();
                        }

                        else {
                            Toast.makeText(getApplicationContext(), "Tiket Anda Sudah Berhasil Dipesan !", Toast.LENGTH_LONG).show();
                            intent.putExtra("daerahTujuan", spinnerTujuan.getSelectedItem().toString());
                            intent.putExtra("tanggalBerangkat", buttonDatePicker.getText().toString());
                            intent.putExtra("waktuBerangkat", buttonTimePicker.getText().toString());
                            intent.putExtra("tanggalPulang", buttonDatePickerPulang.getText().toString());
                            intent.putExtra("waktuPulang", buttonTimePickerPulang.getText().toString());
                            intent.putExtra("jumlahTiket", txtJumlahTiket.getText().toString());
                            intent.putExtra("hargaTotal", hargaTotal.toString());
                            intent.putExtra("saldo", txtSaldo.getText().toString());
                            intent.putExtra("sisaSaldo", sisaSaldo.toString());
                            MainActivity.this.startActivity(intent);
                        }
                    }

                    else if(spinnerTujuan.getSelectedItem().equals("Karawang (Rp 90.000)")){
                        hargaTiket = 90000;
                        hargaTotal = a*hargaTiket*jumlahTiket;
                        Integer sisaSaldo = jumlahSaldo - (hargaTotal);
                        if(jumlahSaldo - (hargaTotal) < 0){
                            AlertDialog.Builder alertBuild = new AlertDialog.Builder(MainActivity.this);
                            alertBuild.setMessage("Mohon Maaf, Saldo Anda Tidak Cukup!").setNegativeButton(
                                    "Coba Lagi", null).create().show();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Tiket Anda Berhasil Dipesan!", Toast.LENGTH_LONG).show();
                            intent.putExtra("daerahTujuan", spinnerTujuan.getSelectedItem().toString());
                            intent.putExtra("tanggalBerangkat", buttonDatePicker.getText().toString());
                            intent.putExtra("waktuBerangkat", buttonTimePicker.getText().toString());
                            intent.putExtra("tanggalPulang", buttonDatePickerPulang.getText().toString());
                            intent.putExtra("waktuPulang", buttonTimePickerPulang.getText().toString());
                            intent.putExtra("jumlahTiket", txtJumlahTiket.getText().toString());
                            intent.putExtra("hargaTotal", hargaTotal.toString());
                            intent.putExtra("saldo", txtSaldo.getText().toString());
                            intent.putExtra("sisaSaldo", sisaSaldo.toString());
                            MainActivity.this.startActivity(intent);
                        }
                    }
                }
                catch (Exception e){
                    AlertDialog.Builder alertBuild = new AlertDialog.Builder(MainActivity.this);
                    alertBuild.setMessage("Mohon Lengkapi Data yang Diatas!").setNegativeButton(
                            "Retry", null).create().show();
                }
            }
        });
    }
}




